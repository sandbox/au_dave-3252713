<?php

namespace Drupal\content_lock_report\Controller;

use Drupal\Core\Controller\ControllerBase;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DateFormatter;

/**
 * Provides Content Lock report.
 */
class ContentLockReport extends ControllerBase {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   *   The database service.
   */
  protected Connection $database;

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity_type.manager service.
   */
  protected $entityTypeManager;

  /**
   * The date.formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   *   The date.formatter service.
   */
  protected $dateFormatter;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity_type.manager service.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The date.formatter service.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entityTypeManager, DateFormatter $dateFormatter) {
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('date.formatter')
    );
  }

  /**
   * Create a report of content locks.
   */
  public function report() {
    $query = $this->database->select('content_lock', 'c');
    $query->leftJoin('users_field_data', 'u', '%alias.uid = c.uid');
    $query->fields('c')
      ->fields('u', ['name']);
    $locks = $query->execute()->fetchAll();

    $results = [];
    foreach ($locks as $lock) {
      $username = $this->entityTypeManager->getStorage('user')->load($lock->uid);
      $entity = $this->entityTypeManager->getStorage($lock->entity_type)->load($lock->entity_id);
      $edit_link = Link::fromTextAndUrl($entity->getTitle(), Url::fromRoute("entity." . $lock->entity_type . ".edit_form", [$lock->entity_type => $entity->id()]))->toString();

      $break_lock = Link::fromTextAndUrl($this->t('Break Lock'),
        Url::fromRoute("content_lock.break_lock." . $lock->entity_type,
          [
            'entity' => $entity->id(),
            'langcode' => $lock->langcode,
            'form_op' => $lock->form_op,
          ])
      )->toString();

      $results[] = [
        [
          'data' => [
            '#type' => 'markup',
            '#markup' => $edit_link,
          ],
        ],
        [
          'data' => [
            '#plain_text' => $username->getDisplayName(),
          ],
        ],
        [
          'data' => [
            '#plain_text' => $this->dateFormatter->format($lock->timestamp, 'medium'),
          ],
        ],
        [
          'data' => [
            '#markup' => '<div class="dropbutton-wrapper dropbutton-single"><div class="dropbutton-widget"><ul class="dropbutton"><li class="manage-display dropbutton-action">' . $break_lock . '</li></ul></div></div>',
          ],
        ],
      ];
    }

    return [
      '#theme' => 'table',
      '#rows' => $results,
      '#header' => ['page', 'user', 'created', 'Break Lock'],
    ];
  }

}
